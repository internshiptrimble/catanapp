import { Link } from "react-router-dom";

function HomeViewNavigation() {
    return (
        <div>
            <ul>
                <li>
                    <Link to='/'>Main Page</Link>
                </li>
                <li>
                    <Link to='/pieces'>Pieces</Link>
                </li>
                <li>
                    <Link to='/actions'>Actions</Link>
                </li>
                <li>
                    <Link to='/scenarios'>Scenarios</Link>
                </li>
                <li>
                    <Link to='/extra'>Extra</Link>
                </li>              
            </ul>
        </div>
    );
}

export default HomeViewNavigation;