import { Route, Routes } from 'react-router';
import './App.css';
import ActionsPage from './components/routes/ActionsPage';

import MainPage from './components/routes/MainPage';
import PiecesPage from './components/routes/PiecesPage';
import ScenariosPage from './components/routes/ScenariosPage';

function App() {
  return (
    <div>
      <Routes>
        <Route path='/' element={<MainPage />}></Route>
        <Route path='/pieces' element={<PiecesPage />}></Route>
        <Route path='/actions' element={<ActionsPage />}></Route>
        <Route path='/scenarios' element={<ScenariosPage />}></Route>
      </Routes>
    </div>
  );
}

export default App;
