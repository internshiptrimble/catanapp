﻿using CatanAPI.Models;
using CatanAPI.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CatanAPI.Services
{
    public interface IPieceCollectionService
    {
        Task<List<Piece>> GetAll();

        Task<Piece> Get(Guid id);

        Task<bool> Create(Piece piece);

        Task<bool> Update(Guid id, Piece piece);

        Task<bool> Delete(Guid id);
    }
}
