﻿using CatanAPI.Models;
using CatanAPI.Pagination;
using CatanAPI.Settings;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CatanAPI.Services
{
    public class PieceCollectionService : IPieceCollectionService
    {
        private readonly IMongoCollection<Piece> _pieces;

        public PieceCollectionService(IMongoDBSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _pieces = database.GetCollection<Piece>(settings.PieceCollectionName);
        }

        public async Task<bool> Create(Piece piece)
        {
            await _pieces.InsertOneAsync(piece);
            return true;
        }

        public async Task<bool> Delete(Guid id)
        {
            var result = await _pieces.DeleteOneAsync(piece => piece.Id == id);
            if(!result.IsAcknowledged && result.DeletedCount == 0)
            {
                return false;
            }
            return true;
        }

        public async Task<Piece> Get(Guid id)
        {
            return (await _pieces.FindAsync(piece => piece.Id == id)).FirstOrDefault();
        }

        public async Task<List<Piece>> GetAll()
        {
            var result = await _pieces.FindAsync(piece => true);
            return result.ToList();
        }

        public async Task<bool> Update(Guid id, Piece piece)
        {
            piece.Id = id;
            var result = await _pieces.ReplaceOneAsync(piece => piece.Id == id, piece);
            if(!result.IsAcknowledged && result.ModifiedCount == 0)
            {
                return false;
            }
            return true;
        }
    }
}
