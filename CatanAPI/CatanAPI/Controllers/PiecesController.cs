﻿using CatanAPI.Models;
using CatanAPI.Pagination;
using CatanAPI.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace CatanAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PiecesController : ControllerBase
    {
        IPieceCollectionService _pieceCollectionService;

        public PiecesController(IPieceCollectionService pieceCollectionService)
        {
            _pieceCollectionService = pieceCollectionService ?? throw new ArgumentNullException(nameof(pieceCollectionService));
        }

        /// <summary>
        ///     Returns a list of all pieces
        /// </summary>
        /// <returns></returns>
        [HttpGet("allpieces")]
        public async Task<IActionResult> GetAllPieces()
        {
            List<Piece> pieces = await _pieceCollectionService.GetAll();
            return Ok(pieces);
        }

        /// <summary>
        ///     Return a piece by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPiece(Guid id)
        {
            if (id == null)
            {
                return BadRequest("Id cannot be null!");
            }

            Piece piece = await _pieceCollectionService.Get(id);
            if (piece == null)
            {
                return NoContent();
            }

            return Ok(piece);
        }

        /// <summary>
        ///     Create a piece
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreatePiece([FromBody] Piece piece)
        {
            if (piece == null)
            {
                return BadRequest("Piece cannot be null!");
            }

            if (await _pieceCollectionService.Create(piece))
            {
                return Ok("Piece successfully created!");
            }

            return NoContent();
        }

        /// <summary>
        ///     Update piece by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="piece"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdatePiece(Guid id, [FromBody] Piece piece)
        {
            if (piece == null)
            {
                return BadRequest("Piece cannot be null!");
            }

            if (await _pieceCollectionService.Update(id, piece))
            {
                return Ok("Piece successfully updated!");
            }

            return NotFound("Piece not found");
        }

        /// <summary>
        ///     Delete piece by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeletePiece(Guid id)
        {
            if (id == null)
            {
                return BadRequest("Id cannot be null!");
            }

            if (await _pieceCollectionService.Delete(id))
            {
                return Ok("Piece successfully deleted!");
            }

            return NotFound("Piece not found!");
        }

        /// <summary>
        ///     Get a specified ammount of items per page
        /// </summary>
        /// <param name="paginationParameters"></param>
        /// <returns></returns>
        [HttpGet("/pieces")]
        public async Task<IActionResult> GetPage([FromQuery] PaginationParameters paginationParameters)
        {
            List<Piece> pieces = await _pieceCollectionService.GetAll();
            var paginationMetadata = new PaginationMetadata(paginationParameters.PageNumber, paginationParameters.ItemsPerPage, pieces.Count());
            Response.Headers.Add("X-Pagination", JsonSerializer.Serialize(paginationMetadata));
            var result = pieces.Skip((paginationParameters.PageNumber - 1) * paginationParameters.ItemsPerPage)
                            .Take(paginationParameters.ItemsPerPage)
                            .ToList();
            return Ok(result);
        }
    }
}
