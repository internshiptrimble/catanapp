﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CatanAPI.Pagination
{
    public class PaginationParameters
    {
        private const int _maxItemsPerPage = 2;
        private int itemsPerPage;

        public int PageNumber { get; set; } = 1;
        public int ItemsPerPage
        {
            get => itemsPerPage; 
            set => itemsPerPage = value > _maxItemsPerPage ? _maxItemsPerPage : value;
        }
    }
}
